<?php

namespace MatiasMuller\Laravel\Searches;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class SearchesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            Commands\MakeSearcher::class,
        ]);

    }

    public function register()
    {
        //
    }
}
