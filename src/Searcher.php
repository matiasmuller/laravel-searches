<?php

namespace MatiasMuller\Laravel\Searches;

use Illuminate\Database\Eloquent\Builder;
use Str;
use Arr;

abstract class Searcher
{
    /**
     * El objeto encargado de gestionar la consulta de búsqueda.
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $query;

    /**
     * Opciones de búsqueda.
     *
     * @var array
     */
    protected $searchOptions;

    /**
     * Parámetros de búsqueda.
     *
     * @var array
     */
    protected $searchParams;

    /**
     * Filtros de búsqueda.
     *
     * @var array
     */
    protected $filters;

    /**
     * Órden de búsqueda.
     *
     * @var array
     */
    protected $sorts;

    /**
     * Atributos globales de búsqueda. Estos atributos son omitidos en la generación
     * de links de paginación
     *
     * @var array
     */
    protected $globalAttributes;

    /**
     * Orden por defecto
     *
     * @var string|null
     */
    protected $defaultSort;

    /**
     * Filtros que van a considerarse en links de paginación.
     * Se compone de todos los parámetros de búsqueda recibidos
     * que pertenecen a los filtros posibles, y no pertenecen
     * a los globales
     *
     * @var array
     */
    protected $validFilters = [];

    /**
     * Configuración de paginación por defecto para esta búsqueda
     *
     * @var int|null
     */
    public $pagination = null;

    /**
     * Atributos que se omiten el las urls de paginación
     * - Si viene sólo un attr, se espera su valor desde el request
     * - Si viene attr => valor, el valor precede cualquier input
     *
     * @var array
     */
    protected $globals = [];

    /**
     * Nombre del atributo que será leído para procesar el orden
     *
     * @var string
     */
    public $sortAttribute = 'sort';

    /**
     * Métodos de filtros personalizados
     *
     * @var array
     */
    protected static $customFilters = [];


    /**
     * Crea una nueva instancia del Searcher.
     *
     * @param  array  $queryParams
     * @param  array  $searchOptions
     * @return void
     */
    public function __construct(array $queryParams, array $searchOptions = [])
    {
        // Prepara la instancia
        $modelClass = $this->getModelClass();
        $this->query = (new $modelClass())->newQuery();
        $this->searchOptions = $searchOptions;

        // Prepara los datos de búsqueda junto con los globales
        $this->searchParams = $this->preparedQueryParams($queryParams);

        // Prepara los filtros
        $this->filters = $this->preparedFilters();

        // Prepara los ordenamientos
        $this->sorts = $this->preparedSorts();
    }


    /**
     * Crea una nueva instancia del Searcher.
     * Conveniente para poder encadenar métodos de instancia
     * luego de la creación, a diferencia de la sintaxis `new Searcher()`
     * que no lo permite.
     *
     * @param  array  $queryParams
     * @param  array  $searchOptions
     * @return \MatiasMuller\Laravel\Searches\Searcher
     */
    public static function create(array $queryParams, array $searchOptions = [])
    {
        return new static($queryParams, $searchOptions);
    }

    /**
     * Clase del modelo para realizar la búsqueda
     *
     * @return  string  Eloquent Model Class
     */
    abstract protected function getModelClass();


    /**
     * Filtros disponibles. Las claves son los parámetros de entrada,
     * y los valores son el/los atributo/s del modelo que se comparan
     * con el valor de entrada. A continuación algunos ejemplos.
     *
     * ```php
     * return [
     *     // Valores sin claves se buscan según la función por defecto
     *     'single_param',
     *     // El ejemplo anterior es el equivalente al ejemplo sigiuente
     *     'single_param' => 'single_param',
     *     // Valores con claves que refieren a otros atributos:tipos de búsqueda
     *     'another_param' => 'another_param1|another_param2:query_type',
     *     // Valores con claves se buscan según la función definida manualmente
     *     'complex_param' => function($query, $value) {
     *         $query->where('complex_param', $value);
     *     },
     * ];
     * ```
     *
     * @return array
     */
    protected function filters()
    {
        return [];
    }


    /**
     * Mapa de ordenamientos disponibles. Las claves son los atributos
     * de ordenamiento disponibles, y los valores son los campos de referencia,
     * en el sentido y orden de evaluación indicado
     *
     * ```php
     * return [
     *     // Valores sin claves se ordenan en base a ese atributo
     *     'single_param',
     *     // El ejemplo anterior es el equivalente al ejemplo sigiuente
     *     'single_param' => 'single_param',
     *     // Claves con valores de signo se ordenan en base a ese atributo en
     *     // sentido inverso
     *     'another_param' => '-',
     *     // El ejemplo anterior es el equivalente al ejemplo sigiuente
     *     'another_param' => '-another_param',
     *     // Claves con valores de atributos, y opcionalmente signo, se ordenan
     *     // en base a dichos atributos y sus sentidos, en el orden colocado
     *     'param' => '-another_param1|another_param2',
     *     // TODO: Hacer que se pueda ordenar por campos de relaciones:
     *     //      'related_param' => 'relation.param1|relation.param2',
     * ];
     * ```
     *
     * @return array
     */
    protected function sorts()
    {
        return [];
    }


    /**
     * Configuración inicial de consulta
     *
     * TODO: Configurar los distintos features (Eager loading, joins,
     *       groups, etc...) por medio de propiedades / métodos
     *       especializados
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    protected function prepare($query)
    {
        // Operaciones de Builder aplicadas al $query
    }


    /**
     * Da forma al array de filtros para que sea sencillo de procesar
     *
     * @param  array  $queryParams
     * @return array
     */
    protected function preparedQueryParams($queryParams)
    {
        $globals = $this->searchOptions['globals'] ?? $this->globals;

        $this->globalAttributes = array_keys(H::arrayKeyify($globals));

        $globalValues = array_filter($globals, function ($key) {
            return !is_integer($key);
        }, ARRAY_FILTER_USE_KEY);

        return array_merge($queryParams, $globalValues);
    }


    /**
     * Devuelve un array basado en los filtros especificados preparado
     * para ser utilizado en la búsqueda
     *
     * @return array
     */
    protected function preparedFilters()
    {
        return H::arrayKeyify($this->filters());
    }


    /**
     * Devuelve un array basado en los órdenes especificados preparado
     * para ser utilizado en la búsqueda
     *
     * @return array
     */
    protected function preparedSorts()
    {
        return collect(H::arrayKeyify($this->sorts()))
            ->map(function ($item, $key) {
                return $item === '-' ? "-$key" : $item;
            })->toArray();
    }


    /**
     * Realiza la búsqueda
     *
     * @return \Illuminate\Database\Eloquent\Collection[]
     */
    public function search()
    {
        // Aplica la preparación inicial del query definida manualmente
        $this->prepare($this->query);

        // Aplica los filtros
        $this->applyFilters();

        // Aplica los ordenamientos
        $this->applySorts();

        return $this->getResults();
    }


    /**
     * Aplica los filtros
     *
     * @return void
     */
    protected function applyFilters()
    {
        $filters = $this->filters;

        $searchParams = $this->searchParams;

        foreach ($searchParams as $param => $value) { //>> REVISAR
            // Si hay valor y el parámetro es clave del array
            if (!is_null($value) && array_key_exists($param, $filters)) {
                $filter = &$filters[$param];

                if (is_callable($filter)) {
                    // Si es una función, se ejecuta
                    $filter($this->query, $value);
                } elseif (is_array($filter)) {
                    // Si es un array, refiere a uno o varios parámetros
                    $this->applyDefaultFilters($this->query, $filter, $value);
                } elseif (is_string($filter)) {
                    // Si es un string, interpreta como array separado por ´|´
                    $this->applyDefaultFilters($this->query, explode('|', $filter), $value);
                }

                // Marca el filtro como válido siempre y cuando no esté marcado
                // como global
                if (!in_array($param, $this->globalAttributes)) {
                    $this->validFilters[$param] = $value;
                }
            }
        }
    }

    /**
     * Se encarga de procesar secuencias de filtros
     * pre-definidos por defecto
     */
    protected function applyDefaultFilters($query, $filter, $value)
    {
        if (count($filter) === 1) {
            $this->defaultFilterAction($query, $filter[0], $value);
        } else {
            $query->where(function (Builder $subQuery) use ($filter, $value) {
                $firstFilter = array_shift($filter);
                $this->defaultFilterAction($subQuery, $firstFilter, $value);
                foreach ($filter as $anotherFilter) {
                    $this->defaultFilterAction($subQuery, $anotherFilter, $value, true);
                }
            });
        }
    }

    /**
     * Resuelve el tipo de función por defecto, en principio
     * si es campo de tabla del modelo, o de relación
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $param
     * @param  mixed  $value
     * @param  boolean  $or
     */
    protected function defaultFilterAction($query, $param, $value, $or = false)
    {
        if (preg_match('/(\w+)\.(.+)/', $param, $match)) {
            $relation = $match[1];
            $relatedParam = $match[2];

            $this->defaultRelationFilter($query, $relation, $relatedParam, $value, $or);
        } else {
            $this->defaultFilter($query, $param, $value, $or);
        }
    }

    /**
     * Aplica el filtro de relación por defecto
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $relation
     * @param  string  $param
     * @param  mixed  $value
     * @param  boolean  $or
     */
    protected function defaultRelationFilter($query, $relation, $param, $value, $or = false)
    {
        $whereHas = $or ? 'orWhereHas' : 'whereHas';
        $query->$whereHas($relation, function (Builder $query) use ($param, $value) {
            $this->defaultFilterAction($query, $param, $value);
        });
    }


    /**
     * Aplica el filtro de campo por defecto
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $param
     * @param  mixed  $value
     * @param  boolean  $or
     */
    protected function defaultFilter($query, $param, $value, $or = false)
    {
        $paramParts = explode(':', $param);
        $attr = $paramParts[0];
        $where = $or ? 'orWhere' : 'where';

        $queryType = isset($paramParts[1])
            ? Str::camel($paramParts[1])
            : 'default';

        // Resuelve el método
        if (array_key_exists($queryType, $customFilters = self::$customFilters)) {
            $customFilters[$queryType]($query, $where, $attr, $value);
        } else {
            $queryMethodName = 'query' . ucfirst($queryType);

            // TODO: Capturar excepción cuando no existe el método
            $this->$queryMethodName($query, $where, $attr, $value);
        }
    }


    /**
     * Aplica los ordenamientos
     *
     * @return void
     */
    protected function applySorts()
    {
        $sorts = $this->sorts;

        $searchParams = $this->searchParams;

        $sortAttribute = $this->sortAttribute;

        if ($sortParam = Arr::get($searchParams, $sortAttribute)) {
            $tryAddToValidFilters = true;
        } else {
            $tryAddToValidFilters = false;
            $sortParam = $this->defaultSort;
        }

        if ($sortParam) {
            if (preg_match('/(-?)(\w+)/', $sortParam, $match)) {
                $asc = $match[1] !== '-';
                $sortRef = $match[2];

                // Si hay valor y el parámetro es clave del array
                if (array_key_exists($sortRef, $sorts)) {
                    $sort = &$sorts[$sortRef];

                    if (is_array($sort)) {
                        // Si es un array, refiere a uno o varios parámetros
                        $this->applyDefaultSorts($sort, $asc);
                    } elseif (is_string($sort)) {
                        // Si es un string, interpreta como array separado por ´|´
                        $this->applyDefaultSorts(explode('|', $sort), $asc);
                    }

                    // Marca el filtro como válido siempre y cuando no esté marcado como global,
                    // y venga del request (para evitar guardar un eventual ordenado por defecto)
                    if ($tryAddToValidFilters && !in_array($sortAttribute, $this->globalAttributes)) {
                        $this->validFilters[$sortAttribute] = $sortParam;
                    }
                }
            }
        }
    }

    /**
     * Se encarga de procesar secuencias de ordenamientos
     * pre-definidos por defecto
     *
     * @return void
     */
    protected function applyDefaultSorts($sort, $asc)
    {
        foreach ($sort as $attrParam) {
            preg_match('/(-?)(\w+)/', $attrParam, $match);
            $attrAsc = $match[1] !== '-';
            $attrRef = $match[2];

            $direction = ($attrAsc xor $asc) ? 'desc' : 'asc';

            $this->query->orderBy($attrRef, $direction);
        }
    }


    /**
     * Una vez aplicados los filtros y ordenamientos, obtiene el
     * resultado
     *
     * @return \Illuminate\Database\Eloquent\Collection[]
     */
    protected function getResults()
    {
        $pagination = $this->searchOptions['pagination'] ?? $this->pagination;

        if ($pagination !== false) {
            return $this->query
                ->paginate($pagination)
                ->appends($this->validFilters);
        } else {
            return $this->query->get();
        }
    }


    public static function extendFilters($name, $callback)
    {
        // TODO: Hacer una validación de la interface del callback
        self::$customFilters[$name] = $callback;
    }


    /*
    |--------------------------------------------------------------------------
    | Tipos de consultas
    |--------------------------------------------------------------------------
    | Todos los métodos con la siguiente declaración:
    |
    | ```php
    | protected function queryNombre($query, $where, $attr, $value)
    | ```
    |
    | Responderán al tipo de filtro `:nombre` en el array `$this->filters()`.
    | A continuación, los tipos de filtro base predefinidos. Pueden realizarse
    | nuevos métodos o incluso sobreescribir los existentes en cada clase
    | derivada.
    |
    | TODO: Implementar la extensión global de tipos de filtro para poder
    |       realizarse desde un Service Provider
    |--------------------------------------------------------------------------
    */

    /**
     * Aplica el filtro de comparación por coincidencia exacta
     * `$attr = $value` (filtro por defecto)
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $where
     * @param  string  $attr
     * @param  mixed  $value
     */
    protected function queryDefault($query, $where, $attr, $value)
    {
        $query->$where($attr, $value);
    }

    /**
     * Aplica el filtro de comparación `$attr LIKE '%$value%'`
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $where
     * @param  string  $attr
     * @param  mixed  $value
     */
    protected function queryString($query, $where, $attr, $value)
    {
        $query->$where($attr, 'like', "%$value%");
    }

    /**
     * Aplica el filtro de comparación `$attr > $value`
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $where
     * @param  string  $attr
     * @param  mixed  $value
     */
    protected function queryGt($query, $where, $attr, $value)
    {
        $query->$where($attr, '>', $value);
    }

    /**
     * Aplica el filtro de comparación `$attr >= $value`
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $where
     * @param  string  $attr
     * @param  mixed  $value
     */
    protected function queryGte($query, $where, $attr, $value)
    {
        $query->$where($attr, '>=', $value);
    }

    /**
     * Aplica el filtro de comparación `$attr < $value`
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $where
     * @param  string  $attr
     * @param  mixed  $value
     */
    protected function queryLt($query, $where, $attr, $value)
    {
        $query->$where($attr, '<', $value);
    }

    /**
     * Aplica el filtro de comparación `$attr <= $value`
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $where
     * @param  string  $attr
     * @param  mixed  $value
     */
    protected function queryLte($query, $where, $attr, $value)
    {
        $query->$where($attr, '<=', $value);
    }
}
