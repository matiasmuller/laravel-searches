<?php

namespace MatiasMuller\Laravel\Searches\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeSearcher extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:searcher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea una clase searcher asociada a un modelo';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Searcher';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->resolveStubPath('/stubs/searcher.stub');
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath($stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Searches';
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);

        return $this->replaceModelclass($stub, $name);


    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceModelclass(&$stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        $modelclass = preg_replace('/Search$/', '', $class);

        $search = ['DummyModelClass', '{{ modelclass }}', '{{modelclass}}'];

        return str_replace($search, $modelclass, $stub);
    }

}
