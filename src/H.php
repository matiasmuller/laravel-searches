<?php

namespace MatiasMuller\Laravel\Searches;

class H
{
    public static function arrayKmap()
    {
        $args = func_get_args();

        $map = call_user_func_array('array_map', $args);

        $result = [];
        foreach ($map as $elem) {
            $key = array_keys($elem)[0];
            $result[$key] = $elem[$key];
        }

        return $result;
    }

    public static function arrayKeyify($array)
    {
        return array_kmap(function ($key, $value) {
            return [is_integer($key) ? $value : $key => $value];
        }, array_keys($array), array_values($array));
    }
}
