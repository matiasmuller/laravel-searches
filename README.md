Laravel Searches
================================================================================

Manage DB queries very easily in [Laravel](https://laravel.com/) using simple search classes similar to form requests.

Table of Contents
--------------------------------------------------------------------------------

- [Getting Started](#getting-started)

Getting Started
--------------------------------------------------------------------------------

***Note:** If you think this documentation can be improved in any way, please [edit this file](https://github.com/matiasmuller/laravel-searches/edit/master/README.md) and make a pull request.*

### 1. Install Laravel Searches

Run this at the command line:

```bash
composer require matiasmuller/laravel-searches
```

This will update `composer.json` and install the package into the `vendor/` directory.


